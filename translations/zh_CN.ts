<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../core/systemnotifiessender.cpp" line="78"/>
        <source>The current system is detected to be stuck, we recommend that you enable the hierarchical freezing function</source>
        <translatorcomment>检测到当前系统可能存在卡顿的情况，建议开启分级冻结功能</translatorcomment>
        <translation>检测到当前系统可能存在卡顿的情况，建议开启分级冻结功能</translation>
    </message>
    <message>
        <location filename="../core/systemnotifiessender.cpp" line="79"/>
        <source>Open</source>
        <translatorcomment>开启</translatorcomment>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="../base/notifydbusinterface.cpp" line="78"/>
        <source>Settings</source>
        <translatorcomment>设置</translatorcomment>
        <translation>设置</translation>
    </message>
</context>
</TS>
